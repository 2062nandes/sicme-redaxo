import { menuToggle } from "./modules/menu"
import { getdate } from "./modules/date"
import { edTabs } from "./modules/tabs"
import { effectsEntry } from "./modules/effects-entry"

import { vLazyLoad } from "./libraries/vanillaLazyLoad"
import { galleryBox } from "./libraries/baguettebox"
import { sReveal } from "./libraries/scrollReveal"
import { tns } from './libraries/tiny'
// import { indicatorScroll } from "./modules/indicator-scroll"
// import { videoSize } from "./modules/video"

// Slider VanillaJS (https://github.com/ganlanyuan/tiny-slider)
// import { tns } from "tiny-slider/src/tiny-slider.module"
// import { options_slider } from "./modules/slider"

// import Headroom from 'headroom.js/dist/headroom.min'
// import { options_header } from "./libraries/headroom"

// import Data Vue
import { contactform } from './data/contactform'
import { menuinicio, mainmenu} from './data/menus'
import { path_media, path_page } from "./data/routes"

import Vue from 'vue/dist/vue.min'
// import Vue from 'vue/dist/vue'
import VueResource from 'vue-resource/dist/vue-resource.min'
Vue.use(VueResource);

// Vue Components
import { googleMap } from './components/googlemaps'
Vue.component('google-map', googleMap)

const sicme = new Vue({
  el: '#sicme',
  data: {
    path_media,
    path_page,
    menuinicio,
    mainmenu,
    formSubmitted: false,
    vue: contactform,
  },
  beforeMount: function() {
  },
  mounted: function () {
    /*Vanilla Lazy Load */
    vLazyLoad()
    /* Images Desplegables  */
    galleryBox()
    /* CAROSUEL PRE-FOOTER */
    let slider = tns({
      container: '.carousel-slider',
      speed: 1200,
      autoplay: true,
      autoplayTimeout: 2500,
      // autoplayHoverPause: true,
      responsive: {
        "0": {
          items: 2
        },
        "640": {
          items: 3
        },
        "950": {
          items: 4
        },
        "1124": {
          items: 5
        },
        "1240": {
          items: 6
        },
        "1440": {
          items: 7
        }
      },
    });
    /*CAROUSEL NOVEDADES */
    let carouselNovedades = tns({
      container: '.carousel-novedades',
      speed: 1200,
      autoplay: true,
      autoplayTimeout: 2500,
      // autoplayHoverPause: true,
      responsive: {
        "0": {
          items: 2
        },
        "640": {
          items: 3
        },
        "950": {
          items: 4
        },
        "1124": {
          items: 5
        },
        "1240": {
          items: 6
        },
        "1440": {
          items: 7
        }
      },
    });

    window.edTabs = edTabs
    // const slider = tns(options_slider)
    menuToggle()
    getdate()
    // indicatorScroll()  
    effectsEntry()
    sReveal()
  },
  methods: {
    isFormValid: function () {
      return this.nombre != ''
    },
    clearForm: function () {
      this.vue.nombre = ''
      this.vue.email = ''
      this.vue.telefono = ''
      this.vue.movil = ''
      this.vue.direccion = ''
      this.vue.ciudad = ''
      this.vue.mensaje = ''
      this.vue.formSubmitted = false
    },
    submitForm: function () {
      if (!this.isFormValid()) return
      this.formSubmitted = true
      this.$http.post('../../mail.php', { vue: this.vue }).then(function (response) {
        this.vue.envio = response.data
        this.clearForm()
      }, function () { })
    },
    acordionVue: function (id) {
      if (this.mainmenu[id].state == true) {
        this.mainmenu[id].state = false
      } else {
        for (let i = 0; i < this.mainmenu.length; i++) {
          this.mainmenu[i].state = false;
        }
        this.mainmenu[id].state = true;
      }
    }
  }
})