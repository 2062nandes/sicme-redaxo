/*
  |--------------------------------------------------------------------------
  | baguetteBox.js - Simple and easy to use lightbox script written in pure JavaScript
  |--------------------------------------------------------------------------
  |
  | npm install baguettebox.js --save
  |
  */
import baguetteBox from 'baguettebox.js';

export const galleryBox = () => {
  baguetteBox.run('#gallery');
}