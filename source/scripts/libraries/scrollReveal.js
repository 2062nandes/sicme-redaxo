import ScrollReveal from 'scrollreveal/dist/scrollreveal.min'

export const sReveal = () => {
  window.sr = ScrollReveal({ reset: true });
  sr.reveal('.main-main__title', { duration: 900, delay: 20 });
  // sr.reveal('.bar');
  sr.reveal('#gallery-item', { duration: 1000 })
}