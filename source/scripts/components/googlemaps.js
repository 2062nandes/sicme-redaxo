/*
  |--------------------------------------------------------------------------
  | Google Maps Javascript API
  |--------------------------------------------------------------------------
  | Add the api script of google maps with your ´YOUR_API_KEY´ before the script.js from project
  | Example Pug: ´script(src= "https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY")´
  |
  */
export const googleMap = {
  template: `<div class="google-map" :id="mapName"></div>`,
  data() {
    return {
      mapName: 'map',
      zoom: 16,
      center: {
        latitude: -17.394890,
        longitude: -66.166367
      },
      markerCoordinates: [
        {
          latitude: -17.394890,
          longitude: -66.166367,
          title: `<div id="content">
                      <h2 id="firstHeading" class="firstHeading">SICME ELECTRIK</h2>
                      <div id="bodyContent">
                        <p><b>Descripción:</b> Materiales eléctricos de alta y baja presión.</p>
                        <p><b>Dirección:</b> Calle General Achá No. 845<br>entre calle Avaróa y Av. Juana Azurduy de Padilla<br>Telefono: (+591-4) 4582880 / Celular: 76902100<br>E-mail: info@sicme.com.bo</p>
                      </div><hr>
                      <footer><a class="button--cta center" href="https://www.google.com/maps/place/SICME+Electrik/@-17.3949136,-66.1685568,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0x1850b89cc8476bf6!8m2!3d-17.3949136!4d-66.1663681?hl=es" target="_blank">Ver en google maps</a></footer>
                    </div>`
        }
      ]
    }
  },
  mounted() {
    const element = document.getElementById(this.mapName)
    const options = {
      zoom: this.zoom,
      center: new google.maps.LatLng(this.center.latitude, this.center.longitude)
    }
    const map = new google.maps.Map(element, options);

    this.markerCoordinates.forEach((coord) => {
      const position = new google.maps.LatLng(coord.latitude, coord.longitude);
      const marker = new google.maps.Marker({
        position,
        map
      });

      const infowindow = new google.maps.InfoWindow()
      marker.addListener('click', function () {
        infowindow.close();
      });
      google.maps.event.addListener(marker, 'click',
        (function (marker, coord, infowindow) {
          return function () {
            infowindow.setContent(coord.title);
            infowindow.open(map, marker);
          };
        })(marker, coord, infowindow));
    });
  }
}