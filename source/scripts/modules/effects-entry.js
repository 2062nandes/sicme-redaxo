import { TweenLite } from "gsap"
import { hidden } from "ansi-colors";
export const effectsEntry = () => {
  /* LOGOTIPO PRINCIPAL*/ 
  TweenLite.fromTo('#S', 1, { opacity: 0 }, { css: { opacity: 1 }, delay: 0.5 })
  TweenLite.fromTo('#I', 1, { opacity: 0 }, { css: { opacity: 1 }, delay: 0.8 })
  TweenLite.fromTo('#C', 1, { opacity: 0 }, { css: { opacity: 1 }, delay: 1.1 })
  TweenLite.fromTo('#M', 1, { opacity: 0 }, { css: { opacity: 1 }, delay: 1.4 })
  TweenLite.fromTo('#E', 1, { opacity: 0 }, { css: { opacity: 1 }, delay: 1.7 })
  TweenLite.fromTo('#electrik', 1, { opacity: 0, transform: 'translateX(-100%)' }, { css: { opacity: 1, transform: 'translateX(0)' }, delay: 2 })
  TweenLite.fromTo('#blue', 1.2, { opacity: 0, transform: 'translateX(100%)' }, { css: { opacity: 1, transform: 'translateX(0)' }, delay: 1.7 })
  TweenLite.fromTo('#silver', 1.2, { opacity: 0, transform: 'translateX(-100%)' }, { css: { opacity: 1, transform: 'translateX(0)' }, delay: 1.5 })
  TweenLite.fromTo('#orange', 1.2, { opacity: 0, transform: 'translateX(100%)' }, { css: { opacity: 1, transform: 'translateX(0)' }, delay: 1.7 })

  TweenLite.fromTo('#neon', 1, { opacity: 0 }, { css: { opacity: 1 }, delay: 3 })

  /* MENU INICIO */
  TweenLite.fromTo('#inicio', 1.2, { opacity: 0, transform: 'translateY(-100%) rotateX(-90deg)' }, { css: { opacity: 1, transform: 'perspective(2500px) rotateX(0deg) translateY(0)' }, delay: 1.9 })
  TweenLite.fromTo('#nosotros', 1.2, { opacity: 0, transform: 'translateY(-100%) rotateX(-90deg)' }, { css: { opacity: 1, transform: 'perspective(2500px) rotateX(0deg) translateY(0)' }, delay: 2.1 })
  TweenLite.fromTo('#contactos', 1.2, { opacity: 0, transform: 'translateY(-100%) rotateX(-90deg)' }, { css: { opacity: 1, transform: 'perspective(2500px) rotateX(0deg) translateY(0)' }, delay: 2.3 })
  
  /* MENU PRINCIPAL*/  
  TweenLite.fromTo('.vertical-menu', 1.2, { overflow: 'hidden' }, { css: { overflow: 'auto' }, delay: 4.2 })
  TweenLite.fromTo('#m1', 1.2, { opacity: 0, transform: 'translateX(-100%) rotateY(90deg)' }, { css: { opacity: 1, transform: 'perspective(2500px) rotateY(0deg) translateX(0)' }, delay: 2.4 })
  TweenLite.fromTo('#m2', 1.2, { opacity: 0, transform: 'translateX(-100%) rotateY(90deg)' }, { css: { opacity: 1, transform: 'perspective(2500px) rotateY(0deg) translateX(0)' }, delay: 2.5 })
  TweenLite.fromTo('#m3', 1.2, { opacity: 0, transform: 'translateX(-100%) rotateY(90deg)' }, { css: { opacity: 1, transform: 'perspective(2500px) rotateY(0deg) translateX(0)' }, delay: 2.6 })
  TweenLite.fromTo('#m4', 1.2, { opacity: 0, transform: 'translateX(-100%) rotateY(90deg)' }, { css: { opacity: 1, transform: 'perspective(2500px) rotateY(0deg) translateX(0)' }, delay: 2.7 })
  TweenLite.fromTo('#m5', 1.2, { opacity: 0, transform: 'translateX(-100%) rotateY(90deg)' }, { css: { opacity: 1, transform: 'perspective(2500px) rotateY(0deg) translateX(0)' }, delay: 2.8 })
  TweenLite.fromTo('#m6', 1.2, { opacity: 0, transform: 'translateX(-100%) rotateY(90deg)' }, { css: { opacity: 1, transform: 'perspective(2500px) rotateY(0deg) translateX(0)' }, delay: 2.9 })
  TweenLite.fromTo('#m7', 1.2, { opacity: 0, transform: 'translateX(-100%) rotateY(90deg)' }, { css: { opacity: 1, transform: 'perspective(2500px) rotateY(0deg) translateX(0)' }, delay: 3.0 })
}