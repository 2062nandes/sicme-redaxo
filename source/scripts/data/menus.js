export const menuinicio = [
  {
    title: 'Inicio',
    href: '',
    icon: 'icon-home',
    id: 'inicio'
  },
  {
    title: 'Nosotros',
    href: 'nosotros/',
    icon: 'icon-users',
    id: 'nosotros'
  },
  {
    title: 'Contactos',
    href: 'contactos/',
    icon: 'icon-envelope',
    id: 'contactos'
  }
]
export const mainmenu = [
  {
    title: 'Materiales eléctricos',
    href: 'materiales-electricos',
    id: 'm1',
    submenu: [
      {
        title: 'Placas domiciliarias modular',
        href: 'Placas domiciliarias modular'
      },
      {
        title: 'Interruptores',
        href: 'Interruptores'
      },
      {
        title: 'Interruptores intermediarios',
        href: 'Interruptores intermediarios'
      },
      {
        title: 'Tomacorrientes universales',
        href: 'Tomacorrientes universales'
      },
      {
        title: 'Tomacorrientes Schucko',
        href: 'Tomacorrientes Schucko'
      },
      {
        title: 'Tomacorriente aéreo',
        href: 'Tomacorriente aéreo'
      },
      {
        title: 'Adaptador triple',
        href: 'Adaptador triple'
      },
      {
        title: 'Tomas de teléfono',
        href: 'Tomas de teléfono'
      },
      {
        title: 'Tomas para Tv Coaxial',
        href: 'Tomas para Tv Coaxial'
      },
      {
        title: 'Pulsadores y Zumbadores',
        href: 'Pulsadores y Zumbadores'
      },
      {
        title: 'Placas de piso de alto tráfico',
        href: 'Placas de piso de alto tráfico'
      },
      {
        title: 'Cajas de distribución de piso alto tráfico',
        href: 'Cajas de distribución de piso alto tráfico'
      },
      {
        title: 'Cajas de empotrar',
        href: 'Cajas de empotrar'
      },
      {
        title: 'Clavijas recta 10 Amp.',
        href: 'Clavijas recta 10 Amp.'
      },
      {
        title: 'Clavija lateral 10 Amp.',
        href: 'Clavija lateral 10 Amp.'
      },
      {
        title: 'Clavijas industriales',
        href: 'Clavijas industriales'
      },
      {
        title: 'Extensiones eléctricas',
        href: 'Extensiones eléctricas'
      },
      {
        title: 'Zockets para tubos de iluminación',
        href: 'Zockets para tubos de iluminación'
      },
      {
        title: 'Cables canales ranurados',
        href: 'Cables canales ranurados'
      },
      {
        title: 'Conduits eléctricos',
        href: 'Conduits eléctricos'
      },
      {
        title: 'Tubos corrugados',
        href: 'Tubos corrugados'
      },
      {
        title: 'Terminales para cables eléctricos',
        href: 'Terminales para cables eléctricos'
      },
      {
        title: 'Pararrayos',
        href: 'Pararrayos'
      },
      {
        title: 'Aisladores de protección atmosférica',
        href: 'Aisladores de protección atmosférica'
      },
      {
        title: 'Protector filtro de línea(Cortapicos)',
        href: 'Protector filtro de línea(Cortapicos)'
      },
      {
        title: 'Interruptor reversible 30A CR 830',
        href: 'Interruptor reversible 30A CR 830'
      },
      {
        title: 'Flotador interruptor de nivel',
        href: 'Flotador interruptor de nivel'
      },
      {
        title: 'Interruptor de presión de aire y agua',
        href: 'Interruptor de presión de aire y agua'
      }
    ]
  },
  {
    title: 'Cables eléctricos',
    href: 'cables-electricos',
    id: 'm2',
    submenu: [
      {
        title: 'Cables rígidos desnudos',
        href: 'Cables rígidos desnudos'
      },      
      {
        title: 'Cables flexil 750 V',
        href: 'Cables flexil 750 V'
      },      
      {
        title: 'Cables flexibles SILNAX',
        href: 'Cables flexibles SILNAX'
      },      
      {
        title: 'Cables rígidos 750 V',
        href: 'Cables rígidos 750 V'
      },      
      {
        title: 'Cables rígidos SILNAX',
        href: 'Cables rígidos SILNAX'
      },      
      {
        title: 'Cables SILFLEX PP 500 V',
        href: 'Cables SILFLEX PP 500 V'
      },      
      {
        title: 'Cables flexibles SILNAX 0.6 / 1 KV',
        href: 'Cables flexibles SILNAX 0.6 / 1 KV'
      },      
      {
        title: 'Cordones flexibles paralelos SIL 300 V',
        href: 'Cordones flexibles paralelos SIL 300 V'
      },      
      {
        title: 'Cables cristales polarizados SIL',
        href: 'Cables cristales polarizados SIL'
      }
    ]
  },
  {
    title: 'Automatización y control',
    href: 'automatizacion-y-control',
    id: 'm3',
    submenu: [
      {
        title: 'Térmicos domiciliarios',
        href: 'Térmicos domiciliarios'
      },
      {
        title: 'Térmicos Termomagnéticos Industriales',
        href: 'Térmicos Termomagnéticos Industriales'
      },
      {
        title: 'Contactores de potencia',
        href: 'Contactores de potencia'
      },
      {
        title: 'Reles de protección térmica',
        href: 'Reles de protección térmica'
      },
      {
        title: 'Programadores horarios',
        href: 'Programadores horarios'
      },
      {
        title: 'Botonerías de comando',
        href: 'Botonerías de comando'
      },
      {
        title: 'Sensores para empotrar en pared',
        href: 'Sensores para empotrar en pared'
      },
      {
        title: 'Sensores para empotrar en losa y drywall',
        href: 'Sensores para empotrar en losa y drywall'
      },
      {
        title: 'Sensores de sobreponer en techo',
        href: 'Sensores de sobreponer en techo'
      },
      {
        title: 'Sensores de presencia de pared MPS - 40F',
        href: 'Sensores de presencia de pared MPS - 40F'
      },
      {
        title: 'Sensores de presencia de pared MPS - 40S',
        href: 'Sensores de presencia de pared MPS - 40S'
      }
    ]
  },
  {
    title: 'Luminarias e iluminación',
    href: 'luminarias-e-iluminacion',
    id: 'm4',
    submenu: [
      {
        title: 'Luminarias tipo tortuga',
        href: 'Luminarias tipo tortuga'
      },
      {
        title: 'Lámparas espiral',
        href: 'Lámparas espiral'
      },
      {
        title: 'Focos led',
        href: 'Focos led'
      },
      {
        title: 'Paneles',
        href: 'Paneles'
      },
      {
        title: 'Paneles de sobreponer',
        href: 'Paneles de sobreponer'
      },
      {
        title: 'Paneles de empotrar',
        href: 'Paneles de empotrar'
      },
      {
        title: 'Tubos led',
        href: 'Tubos led'
      }
    ]
  },
  {
    title: 'Cajas eléctricas',
    href: 'cajas-electricas',
    id: 'm5',
    submenu: [
      {
        title: 'Cajas metálicas industriales',
        href: 'Cajas metálicas industriales'
      }
    ]
  },
  {
    title: 'Nuestras marcas',
    href: 'nuestras-marcas',
    id: 'm6'
  },
  {
    title: 'Otros productos',
    href: 'otros-productos',
    id: 'm7',
    submenu: [
      {
        title: 'Duchas',
        href: 'Duchas'
      },
      {
        title: 'Precintos de nylon',
        href: 'Precintos de nylon'
      },
      {
        title: 'Juegos de herramientas Phoenix contact',
        href: 'Juegos de herramientas Phoenix contact'
      },
      {
        title: 'Interfonos',
        href: 'Interfonos'
      },
      {
        title: 'Video porteros',
        href: 'Video porteros'
      },
      {
        title: 'Escaleras metálicas',
        href: 'Escaleras metálicas'
      },
      {
        title: 'Baliza de señalización',
        href: 'Baliza de señalización'
      }
    ]
  }
]